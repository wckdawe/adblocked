﻿[![Contributors][contributors-shield]][contributors-url]
[![All Contributors](https://img.shields.io/badge/all_contributors-3-orange.svg?style=flat-square)](#contributors)
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]

<br />
<p align="center">
  <h1 align="center">ADBLOCKED</h3>

  <p align="center">
    A definatelly not optimized mass destruction raging simulator
    created in Unreal 4.23 as a Computer Science University Project.
  </p>
  <p align="center">Game Development | [Computer Science Dept., University of Thessaly, Greece](CSUTH)</p>
</p>

## Table of Contents

- [Table of Contents](#table-of-contents)
- [About The Project](#about-the-project)
  - [Inspiration](#inspiration)
  - [Copyrighted Material](#copyrighted-material)
  - [Built with](#built-with)
- [Getting Started](#getting-started)
- [Roadmap](#roadmap)
- [Authors](#authors)
- [Disclaimer](#disclaimer)
- [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->

## About The Project

AdBlocked is a "Windows Simulator" & Smash/Break everything game. Your main character, let's call him Leonard, seems to have caught an adware on his computer causing multiple of 
weird ads to appear as well as ruin his computer system. Leonard, the head of SMC., fueled with anger starts raging on everything and eveyrone causing a total mayhem.

Your main objective is to collect as many points as possible by ruining the whole office and killing as many of your coworkers as possible. You can grab & throw objects, throw punches and pickup some special weapons.

Oh, and by the way, If you manage to do it all without lagging we can say that you definatelly have a great computer! Since this is our first ever game it wasn't optimized at all and it was just created for us to have fun and 
explore the power of Unreal engine... so... ¯\_(ツ)_/¯ 

## Inspiration

So imagine this... first time in class of Game Development. We opened Epic Games Launcher to check it out. What did we see? An Advertisement. Actually... multiple. Meanwhile for some reason i was thinking at that time of the 
game [how to whack your boss](https://www.google.com/search?q=how+to+whack+your+boss).. If you haven't played, you've definatelly lost something in your childhood... so yeah.. try it. Our University computers have the Win7 OS.
A few magic code here and there, a lot of blueprints & design, 0 time for optimization or happiness and poof! A Game now has been created and it is avaliable for you to try and tell us what you think.

## Copyrighted Material

There are multiple copyrighted materials contained inside the project (such as Windows Icons, Program Icons), Musics (such as Toto - Hold the Line) & random assets taken from multiple sources to create our awesome
lagging and unplayable experience. If you by any chance are the owner of any copyrighted material inside this project and wish that it gets removed please contact us in advance and we will sort everything out. We will respect 
your decision and promptly remove the copyrighted content. We included that content in the first place as we have no experience in the field and just attempted to have fun under fair use and share our creation with random fellow
students. If you feel like we violated your rights by including your work inside our project we will remove it as soon as we are aware of that.

### Built with
 - [Unreal Engine 4.23](https://www.unrealengine.com/)
 - A lot of blueprints
 - Thinking of Grigorakis (our friend that is leaving our University)


<!-- ROADMAP -->

## Roadmap

There are currently **no plans for completing or expanding** the project by the original authors ([see below](#authors)). Nonetheless, any [contribution](#contributing) to the project is always welcome.

See the [open issues][issues-url] for a list of proposed features (and known issues).

PS: If you figure a way to optimize the game it would be really intresting for us to see your design choices and learn from them.


## Authors
- **Dimitriadis Vasileios** (WckdAwe) --  ( [Website]( http://wckdawe.com) | [Github](https://github.com/wckdawe) )
- **Kouskouras Taxiarchis** (TheNotoriousCS) -- ( [Github](https://github.com/TheNotoriousCS) )

And special thanks to [Papageorgiou Grigorios](https://github.com/TheFamousFurious) for inspiring us with his awesome face and enriching our 4 years of University with amazing experiences.


<!-- Disclaimer -->

## Disclaimer

All the information on this repository is provided in good faith, however we make no representation or warranty of any kind, express or implied, regarding the accuracy, adequacy, validity, reliability, availability or completeness 
of any information - see the [DISCLAIMER.md][DISCLAIMER] file for more details.

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

Hats off to any person whom contributed to this Project, formally or informally. This couldn't be possible without the assistance of these people.

While we can't credit everyone because we did not keep track of the names, projects, assets, music information, we certainly love you guys.
Without you our game would be dull. Because of you unamed heroes we had the power to push forward and finish the game.

### Assets
- [Arbitary Studio](https://forums.unrealengine.com/community/community-content-tools-and-tutorials/31690-free-various-models)
- [Dentist office](https://dan-jones.itch.io/realistic-dentist-office)
- [FPS Weapon Bundle](https://www.unrealengine.com/marketplace/en-US/product/fps-weapon-bundle#:~:text=7%20weapons%20and%204%20attachments%20optimized%20for%20FPS.&text=This%20weapon%20pack%20is%20optimized,are%20included%20for%20all%20firearms)
- [Free Furniture Pack](https://www.unrealengine.com/marketplace/en-US/product/a4907129f69c44a892f76782489736ab)
- [Group5Museum](https://group5museum.itch.io/sci-fi-museum-pack-ue4)
- [Infinity Blade Effects](https://www.unrealengine.com/marketplace/en-US/product/infinity-blade-effects#:~:text=v%3DVMbPHuU1KRs-,Infinity%20Blade%3A%20Effects%20includes%20visual%20effects%20ranging%20from%20fire%20and,Unreal%20Engine%20community%20for%20free!)
- [Interactive Lights](https://www.unrealengine.com/marketplace/en-US/product/interactive-lights-system)
- [OfficeSpace]
- [SammiesBar](https://adamfautley.itch.io/sammies-bar)

### Music
- [Toto - Hold the Line](https://www.youtube.com/watch?v=htgr3pvBr-I)
- [Taking a Beating - Ethan Meixsell](https://www.youtube.com/watch?v=1LbGUxPuupc)
- [Anttis Instrumentals](https://www.soundclick.com/artist/default.cfm?bandid=1277008)

### Other sites
- [Itch.io](http://itch.io)
- [Turbosquid](https://turbosquid.com)
- [Stack Overflow](http://stackoverflow.com) - Obviously

And many more unheard heroes!

[CONTRIBUTING]: https://github.com/WckdAwe/AdBlocked/blob/master/CONTRIBUTING.md
[DISCLAIMER]: https://github.com/WckdAwe/AdBlocked/blob/master/DISCLAIMER.md



<!-- Helpful Links & People -->
[CSUTH]: http://cs.uth.gr/

<!-- othneildrew's Best-README-Template -->
[contributors-shield]: https://img.shields.io/github/contributors/WckdAwe/AdBlocked.svg?style=flat-square
[contributors-url]: https://github.com/WckdAwe/AdBlocked/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/WckdAwe/AdBlocked.svg?style=flat-square
[forks-url]: https://github.com/WckdAwe/AdBlocked/network/members
[stars-shield]: https://img.shields.io/github/stars/WckdAwe/AdBlocked.svg?style=flat-square
[stars-url]: https://github.com/WckdAwe/AdBlocked/stargazers
[issues-shield]: https://img.shields.io/github/issues/WckdAwe/AdBlocked.svg?style=flat-square
[issues-url]: https://github.com/WckdAwe/AdBlocked/issues
[license-shield]: https://img.shields.io/github/license/WckdAwe/AdBlockedr.svg?style=flat-square
[license-url]: https://github.com/WckdAwe/AdBlocked/blob/master/LICENSE.md